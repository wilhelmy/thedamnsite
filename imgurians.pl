#!/usr/bin/env perl
use strict;
use warnings;
use Mojolicious::Lite;
use Carp qw/carp croak/;
use Carp::Always;

BEGIN {
	unshift @INC, "../lib";
}
use Mojo::EggdropRPC;

my $rpc = Mojo::EggdropRPC->new(
	password => 'Satan Calculus is coming to town'
);
$rpc->connect(address => '127.0.0.1', port => 1337, timeout => 0);

app->defaults(failed => '');
app->secrets([' blasd 12 371238 1e m1y8rponsvl2	rsvhttp:12hu3pb i']);

get '/login' => 'login';

post '/login' => sub {
	my $c = shift;

	my $user = $c->param('user') || '';
	my $pass = $c->param('pass') || '';

	return $c->render('login') unless $user && $pass;

	$c->render_later;

	$rpc->call(['checkpass', $user, $pass] => sub {
		my ($rpc, $msg) = @_;

		app->log->warn("error authenticating user '$user'") if $msg->[0] eq 'ERROR';

		$c->stash(failed => 0);
		if ($msg->[0] ne 'OK' || $msg->[1] ne '1') {
			$c->stash(failed => 1);
			app->log->info("invalid password for $user");
			return $c->render('login');
		}

		app->log->debug("password ok");
		$c->flash(user => $user);
		$c->continue; # ok.
		return $c->redirect_to('/');
	});

	# return undef immediately, may later be overridden by $c->continue above.
	return undef;
};

under sub {
	my $c = shift;

	return $c->redirect_to('/login') unless $c->flash('user');

	1
};

any '/' => 'index';

#post '/login' => 'index'; # this is supposed to redirect but bleh...

app->start;
